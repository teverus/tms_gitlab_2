import os
from datetime import datetime
from pathlib import Path

from playwright.sync_api import Page

TEXT_TO_SEARCH = os.getenv("TEXT_TO_SEARCH", "hello_world")
FILES = Path("files")


class TestPlaywright:
    def test_google_smoke(self, page: Page):
        host = "https://www.google.com/"
        current_time = int(datetime.now().timestamp())
        page.goto(host)
        page.wait_for_load_state()
        page.locator("//*[@type='search']").fill(TEXT_TO_SEARCH)
        page.keyboard.press("Enter")
        for _ in range(10):
            current_url = page.url
            if current_url != host:
                break
            page.wait_for_timeout(1000)
        page.wait_for_load_state()
        page.screenshot(path=FILES / Path(f"my_screen{current_time}.png"))
